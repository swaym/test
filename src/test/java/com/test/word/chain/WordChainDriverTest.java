package com.test.word.chain;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocalFileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hdfs.MiniDFSCluster;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.test.PathUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class WordChainDriverTest {

    private static final String CLUSTER_1 = "cluster1";

    private File testDataPath;
    private Configuration conf;
    private MiniDFSCluster cluster;
    private FileSystem fs;

    @Before
    public void setUp() throws Exception {
        testDataPath = new File(PathUtils.getTestDir(getClass()),
                "miniclusters");

        System.clearProperty(MiniDFSCluster.PROP_TEST_BUILD_DATA);
        conf = new Configuration();

        File testDataCluster1 = new File(testDataPath, CLUSTER_1);
        String c1Path = testDataCluster1.getAbsolutePath();
        conf.set(MiniDFSCluster.HDFS_MINIDFS_BASEDIR, c1Path);
        cluster = new MiniDFSCluster.Builder(conf).build();

        fs = FileSystem.get(conf);
    }

    @After
    public void tearDown() throws Exception {
        Path dataDir = new Path(
                testDataPath.getParentFile().getParentFile().getParent());
        fs.delete(dataDir, true);
        File rootTestFile = new File(testDataPath.getParentFile().getParentFile().getParent());
        String rootTestDir = rootTestFile.getAbsolutePath();
        Path rootTestPath = new Path(rootTestDir);
        LocalFileSystem localFileSystem = FileSystem.getLocal(conf);
        localFileSystem.delete(rootTestPath, true);
        cluster.shutdown();
    }

    //Task wasn't done in TDD, test isn't finished
    @Test
    public void testClusterWithData() throws Throwable {

        String IN_DIR = "testing/wordchain/input";
        String OUT_DIR = "testing/wordchain/output";
        String DATA_FILE = "sample.txt";
        String START = "cat";
        String END = "dog";

        Path inDir = new Path(IN_DIR);
        Path outDir = new Path(OUT_DIR);

        fs.delete(inDir, true);
        fs.delete(outDir,true);

        // JOB1 - Process the Dictionary File
        Job job = new Job(this.conf);
        job.setJobName("DictionaryProcess");
        job.setJarByClass(DictMR.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);

        job.setNumReduceTasks(1);
        job.getConfiguration().set("START", START);
        job.getConfiguration().set("END", END);

        job.setMapperClass(DictMR.DictMap.class);
        job.setReducerClass(DictMR.Reduce.class);
        job.setCombinerClass(DictMR.Reduce.class);

        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);


        //TODO Mock DistributedCache
        job.addCacheFile((new Path(OUT_DIR
                + "/part-r-00000").toUri()));

        FileInputFormat.setInputPaths(job, new Path(DATA_FILE));
        FileOutputFormat.setOutputPath(job, new Path(OUT_DIR));

        boolean status = job.waitForCompletion(true);

             // JOB2 - Process the Dictionary File in a loop to get the start to end
        // words
        boolean flag = true;
        int iterations = 0;

        String inputfile = OUT_DIR;
        String wcoutputPath = null;

        while (flag) {
            iterations++;
            wcoutputPath = OUT_DIR;

            job = new Job(this.conf);
            job.setJobName("WordChain-" + iterations);
            // job.getConfiguration().set("mapred.job.queue.name", "score2");
            job.setJarByClass(WordChainMR.class);

            job.setOutputKeyClass(Text.class);
            job.setOutputValueClass(Text.class);

            job.setNumReduceTasks(1);

            job.setMapperClass(WordChainMR.WordMap.class);
            job.setReducerClass(WordChainMR.Reduce.class);
            job.setCombinerClass(WordChainMR.Reduce.class);

            job.setInputFormatClass(TextInputFormat.class);
            job.setOutputFormatClass(TextOutputFormat.class);

            job.getConfiguration().set("START", START);
            job.getConfiguration().set("END", END);

            job.addCacheFile((new Path(OUT_DIR
                    + "/part-r-00000").toUri()));
            FileInputFormat.setInputPaths(job, new Path(inputfile));
            FileOutputFormat.setOutputPath(job, new Path(wcoutputPath));

            job.waitForCompletion(true);

        }

        // now check that the output is as expected
        List<String> results = getJobResults(fs, outDir, 11);
        assertTrue(results.contains("cat"));
        assertTrue(results.contains("cog"));
        assertTrue(results.contains("cot"));
        assertTrue(results.contains("dog"));
        assertTrue(results.size() == 4);

        // clean up after test case
        fs.delete(inDir, true);
        fs.delete(outDir,true);
    }




    protected List<String> getJobResults(FileSystem fs, Path outDir, int numLines) throws Exception {
        List<String> results = new ArrayList<String>();
        FileStatus[] fileStatus = fs.listStatus(outDir);
        for (FileStatus file : fileStatus) {
            String name = file.getPath().getName();
            if (name.contains("part-r-00000")){
                Path filePath = new Path(outDir + "/" + name);
                BufferedReader reader = new BufferedReader(new InputStreamReader(fs.open(filePath)));
                for (int i=0; i < numLines; i++){
                    String line = reader.readLine();
                    if (line == null){
                        fail("Results are not what was expected");
                    }
                    results.add(line);
                }
                assertNull(reader.readLine());
                reader.close();
            }
        }
        return results;
    }

}