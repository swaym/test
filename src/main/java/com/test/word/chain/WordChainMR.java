package com.test.word.chain;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

public class WordChainMR {

    public static class Reduce extends Reducer<Text, Text, Text, Text> {
        private boolean found = false;

        @Override
        protected void reduce(final Text key,
                              final java.lang.Iterable<Text> values, final Context context)
                throws IOException, InterruptedException {
            if (this.found) {
                return;
            }

            final Iterator<Text> itr = values.iterator();

            if (key.equals(new Text("0"))) {
                while (itr.hasNext()) {
                    context.write(itr.next(), new Text());
                }
                this.found = true;
            } else {
                while (itr.hasNext()) {
                    context.write(new Text(key), new Text(itr.next()));
                }
            }
        }
    }

    public static class WordMap extends Mapper<LongWritable, Text, Text, Text> {
        private Map<String, List<String>> dictionary = new HashMap<String, List<String>>();
        private String start = null;

        private String end = null;

        private void loadDictionary(final String file) {
            BufferedReader br = null;
            try {
                br = new BufferedReader(new FileReader(new File(file)));
                String strLine;
                while ((strLine = br.readLine()) != null) {
                    final String[] tokens = strLine.split("\t");
                    final String word = tokens[0];

                    // if (word.length() != Map.start.length()) {
                    // continue;
                    // }

                    final String next = tokens[1];
                    if (this.dictionary.containsKey(word)) {
                        this.dictionary.get(word).add(next);
                    } else {
                        final List<String> alnext = new LinkedList<String>();
                        alnext.add(next);
                        // System.out.println("[filter]Word:" + word);
                        this.dictionary.put(word, alnext);
                    }
                }
            } catch (final Exception e) {
                e.printStackTrace();
            } finally {
                if (br != null) {
                    try {
                        br.close();
                    } catch (final IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        @Override
        protected void map(final LongWritable key, final Text value,
                           final Context context) throws IOException, InterruptedException {
            final String line = value.toString().trim();
            final String[] tokens = line.split("\t");
            final String word = tokens[0];
            final String lastword = tokens[tokens.length - 1];
            if ((word != null) && word.equalsIgnoreCase(this.start)) {
                final List<String> alnext = this.dictionary.get(lastword);
                if (alnext == null) {
                    return;
                }
                for (final String next : alnext) {
                    if (next.equalsIgnoreCase(this.end)) {
                        System.out.println("Found");
                        context.getCounter(WordChainConstant.Status.FOUND)
                                .increment(1);
                        context.write(new Text("0"), new Text(line + "\t"
                                + next));
                    } else {
                        if (!line.contains(next)) {
                            context.write(value, new Text(next));
                        }
                    }
                }
            }
        }

        /**
         * Added for JUnit
         *
         * @param dictionary
         */
        public void setDictionary(final Map<String, List<String>> dictionary) {
            this.dictionary = dictionary;
        };

        @Override
        protected void setup(final Context context) throws IOException,
                InterruptedException {
            final Configuration config = context.getConfiguration();
            URI[] cacheFiles;
            try {
                this.start = config.get("START");
                this.end = config.get("END");
                cacheFiles = context.getCacheFiles();
                if (cacheFiles == null) {
                    System.out.println("Cache File should not be null");
                    return;
                }
                final String dictfile = cacheFiles[0].toString();
                this.loadDictionary(dictfile);

                System.out.println("----------Conf---------------");
                System.out.println("Start:" + this.start);
                System.out.println("End:" + this.end);
                System.out.println("dictfile:" + dictfile);
                System.out.println("Dict size:" + this.dictionary.size());
            } catch (final Exception e) {
                e.printStackTrace();
            }
        }
    }

}
