package com.test.word.chain;


import java.util.ArrayList;
import java.util.Arrays;

public class WordChainConstant {

    public static enum Status {
        FOUND;
    }

    private final static Character[] letters = { 'a', 'b', 'c', 'd', 'e', 'f',
            'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
            't', 'u', 'v', 'w', 'x', 'y', 'z' };

    public static final ArrayList<Character> alLetters = new ArrayList<Character>(
            Arrays.asList(WordChainConstant.letters));

    public static String wcfilename = "wordChain.txt";

}