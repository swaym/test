package com.test.word.chain;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.cli2.CommandLine;
import org.apache.commons.cli2.Group;
import org.apache.commons.cli2.Option;
import org.apache.commons.cli2.OptionException;
import org.apache.commons.cli2.builder.ArgumentBuilder;
import org.apache.commons.cli2.builder.DefaultOptionBuilder;
import org.apache.commons.cli2.builder.GroupBuilder;
import org.apache.commons.cli2.commandline.Parser;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.mahout.common.CommandLineUtil;
import org.apache.tools.ant.util.FileUtils;

public class WordChainDriver extends Configured implements Tool {

    public static void main(final String[] args) throws Exception {

        final int res = ToolRunner.run(new Configuration(),
                new WordChainDriver(), args);
        System.exit(res);
    }

    private Configuration conf;

    private String start;

    private String end;
    private String dictionaryFile;

    private String outputName;
    private final String dictDirectoryPrefix = "Dict";
    private final String wcDirectoryPrefix = "WordChain";

    public boolean cpFilestoUserLocation(final String useroutputfile,
                                         final String systemfile) throws IOException {
        System.out.println("Copy file to user location");
        System.out.println("useroutputfile::" + useroutputfile);
        System.out.println("systemfile::" + systemfile);
        final FileUtils fu = FileUtils.getFileUtils();
        fu.copyFile(systemfile, useroutputfile, null, true);

        return true;
    }

    @Override
    public Configuration getConf() {
        return this.conf;
    }

    public final String getDateTime() {
        final DateFormat df = new SimpleDateFormat("yyyy-MM-dd_hh-mm-ss");
        return df.format(new Date());
    }

    @SuppressWarnings("deprecation")
    public int run(final String[] args) throws Exception {
        final DefaultOptionBuilder obuilder = new DefaultOptionBuilder();
        final ArgumentBuilder abuilder = new ArgumentBuilder();
        final GroupBuilder gbuilder = new GroupBuilder();

        final Option startOpt = obuilder
                .withLongName("start")
                .withShortName("s")
                .withRequired(true)
                .withArgument(
                        abuilder.withName("start").withMinimum(1)
                                .withMaximum(1).create())
                .withDescription("starting word").create();

        final Option endOpt = obuilder
                .withLongName("end")
                .withShortName("e")
                .withRequired(true)
                .withArgument(
                        abuilder.withName("end").withMinimum(1).withMaximum(1)
                                .create()).withDescription("ending word")
                .create();

        final Option dictionaryFileOpt = obuilder
                .withLongName("dict")
                .withShortName("d")
                .withRequired(false)
                .withArgument(
                        abuilder.withName("dict").withMinimum(1).withMaximum(1)
                                .create()).withDescription("dictionary file")
                .create();

        final Option outputDirOpt = obuilder
                .withLongName("o")
                .withShortName("o")
                .withRequired(false)
                .withArgument(
                        abuilder.withName("o").withMinimum(1).withMaximum(1)
                                .create())
                .withDescription("output directory option").create();

        final Group group = gbuilder.withName("Options").withOption(startOpt)
                .withOption(endOpt).withOption(dictionaryFileOpt)
                .withOption(outputDirOpt).create();

        System.out.println(group);

        try {
            final Parser parser = new Parser();
            parser.setGroup(group);
            final CommandLine cmdLine = parser.parse(args);

            if (cmdLine.hasOption("help")) {
                CommandLineUtil.printHelp(group);
                return -1;
            }

            this.start = cmdLine.getValue(startOpt).toString().trim();
            this.end = cmdLine.getValue(endOpt).toString().trim();
            this.dictionaryFile = cmdLine.getValue(dictionaryFileOpt)
                    .toString().trim();
            this.outputName = cmdLine.getValue(outputDirOpt).toString().trim();

            System.out.println("start word::" + this.start);
            System.out.println("end word::" + this.end);
            System.out.println("dictionary file path::" + this.dictionaryFile);
            System.out.println("output Directory::" + this.outputName);

            if (this.start.length() != this.end.length()) {
                System.out
                        .println("Both Start & End Words need to be of same length");
                return -1;
            }

        } catch (final OptionException e) {
            System.err.println("Exception : " + e);
            CommandLineUtil.printHelp(group);
            return -1;
        }

        final String parentDirectory = "WORD_CHAIN";
        final String baseDirectory = this.outputName + "/" + parentDirectory;
        if (!FileSystem.get(new Configuration()).isDirectory(
                new Path(baseDirectory))) {
            FileSystem.get(new Configuration()).mkdirs(new Path(baseDirectory));
        }

        // JOB1 - Process the Dictionary File
        Job job = new Job(this.getConf());
        job.setJobName("DictionaryProcess");
        job.setJarByClass(DictMR.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);

        job.setNumReduceTasks(1);
        job.getConfiguration().set("START", this.start);
        job.getConfiguration().set("END", this.end);

        job.setMapperClass(DictMR.DictMap.class);
        job.setReducerClass(DictMR.Reduce.class);
        job.setCombinerClass(DictMR.Reduce.class);

        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);

        job.addCacheFile((new Path(this.dictionaryFile).toUri()));
        final String dictoutputPath = baseDirectory + "/"
                + this.dictDirectoryPrefix;

        if (FileSystem.get(new Configuration()).isDirectory(
                new Path(dictoutputPath))) {
            FileSystem.get(new Configuration()).delete(new Path(baseDirectory),
                    true);
        }

        FileInputFormat.setInputPaths(job, new Path(this.dictionaryFile));
        FileOutputFormat.setOutputPath(job, new Path(dictoutputPath));

        boolean status = job.waitForCompletion(true);

        if (!status) {
            System.out.println("Could Not create the dictionary file");
            return -1;
        }

        // JOB2 - Process the Dictionary File in a loop to get the start to end
        // words
        boolean flag = true;
        int iterations = 0;

        String inputfile = dictoutputPath;
        String wcoutputPath = null;

        while (flag) {
            iterations++;
            wcoutputPath = baseDirectory + "/" + this.wcDirectoryPrefix
                    + this.getDateTime();

            job = new Job(this.getConf());
            job.setJobName("WordChain-" + iterations);
            job.setJarByClass(WordChainMR.class);

            job.setOutputKeyClass(Text.class);
            job.setOutputValueClass(Text.class);

            job.setNumReduceTasks(1);

            job.setMapperClass(WordChainMR.WordMap.class);
            job.setReducerClass(WordChainMR.Reduce.class);
            job.setCombinerClass(WordChainMR.Reduce.class);

            job.setInputFormatClass(TextInputFormat.class);
            job.setOutputFormatClass(TextOutputFormat.class);

            job.getConfiguration().set("START", this.start);
            job.getConfiguration().set("END", this.end);

            job.addCacheFile((new Path(dictoutputPath
                    + "/part-r-00000").toUri()));
            FileInputFormat.setInputPaths(job, new Path(inputfile));
            FileOutputFormat.setOutputPath(job, new Path(wcoutputPath));

            status = job.waitForCompletion(true);
            final org.apache.hadoop.mapreduce.Counter counter = job
                    .getCounters().findCounter(WordChainConstant.Status.FOUND);

            System.out.println("Found Counter:" + counter.getValue());

            if (counter.getValue() > 0) {
                flag = false;
            } else {
                inputfile = wcoutputPath;
            }

            if (!job.isSuccessful()) {
                break;
            }
        }

        if (!flag
                && this.cpFilestoUserLocation(baseDirectory + "/"
                + WordChainConstant.wcfilename, wcoutputPath + "/"
                + "part-r-00000")) {
            System.out.println("Word Chain File Copied at " + baseDirectory
                    + "/" + WordChainConstant.wcfilename);
        } else {
            System.out.println("No Word Chain found!!!!!");
        }

        return 0;
    }

    @Override
    public void setConf(final Configuration conf) {
        this.conf = conf;
    }

}
