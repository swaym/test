package com.test.word.chain;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.util.bloom.BloomFilter;
import org.apache.hadoop.util.bloom.Key;
import org.apache.hadoop.util.hash.Hash;

public class DictMR {

    public static class DictMap extends Mapper<LongWritable, Text, Text, Text> {

        private static final int VECTOR_SIZE = 10485760;
        private static final int NBHASH = 6;
        private static final int HASH_TYPE = Hash.MURMUR_HASH;
        private String start;

        private Set<String> dictionary = new HashSet<String>();
        private BloomFilter bloomFilter = new BloomFilter(DictMap.VECTOR_SIZE,
                DictMap.NBHASH, DictMap.HASH_TYPE);

        private void loadDictionary(final String file, final int len) {
            BufferedReader br = null;
            try {
                br = new BufferedReader(new FileReader(new File(file)));
                String strLine;
                while ((strLine = br.readLine()) != null) {
                    if (strLine.trim().length() == len) {
                        this.dictionary.add(strLine);
                        this.bloomFilter.add(new Key(strLine.getBytes()));
                    }
                }
            } catch (final Exception e) {
                e.printStackTrace();
            } finally {
                if (br != null) {
                    try {
                        br.close();
                    } catch (final IOException e) {
                        e.printStackTrace();
                    }
                }
            }

        }

        @Override
        protected void map(final LongWritable key, final Text value,
                           final Context context) throws IOException, InterruptedException {

            final String word = value.toString();

            // Only work on similar length words
            if (word.length() != this.getStart().length()) {
                return;
            }

            for (int i = 0; i < word.length(); i++) {
                for (int j = 0; j < WordChainConstant.alLetters.size(); j++) {
                    final char[] newString = word.toCharArray();
                    newString[i] = WordChainConstant.alLetters.get(j);
                    final String newword = String.valueOf(newString);
                    if (this.bloomFilter.membershipTest(new Key(newword
                            .getBytes()))
                            && this.dictionary.contains(newword)
                            && !newword.equals(word)) {
                        context.write(value, new Text(newword));
                    }
                }
            }

        };

        public void setBloomFilter(final BloomFilter bloomFilter) {
            this.bloomFilter = bloomFilter;
        }

        public void setDictionary(final Set<String> dictionary) {
            this.dictionary = dictionary;
        }

        @Override
        public void setup(final Context context) {
            final Configuration config = context.getConfiguration();
            final URI[] cacheFiles;
            try {
                cacheFiles = context.getCacheFiles();
                if (cacheFiles == null) {
                    System.out.println("Cache File should not be null");
                    return;
                }
                final String dictfile = cacheFiles[0].toString();
                System.out.println("Dictfile:" + dictfile + ":"
                        + config.get("START"));
                this.setStart(config.get("START"));
                this.loadDictionary(dictfile, config.get("START").length());
                System.out.println("Dict file Size::" + this.dictionary.size());
            } catch (final Exception e) {
                e.printStackTrace();
            }
        }

        public void setStart(String start) {
            this.start = start;
        }

        public String getStart() {
            return start;
        }
    }

    public static class Reduce extends Reducer<Text, Text, Text, Text> {

        @Override
        protected void reduce(final Text key,
                              final java.lang.Iterable<Text> values, final Context context)
                throws IOException, InterruptedException {
            final Iterator<Text> itr = values.iterator();
            while (itr.hasNext()) {
                context.write(new Text(key + "\t" + itr.next()), new Text());
            }
        };
    }

}
